name := "DiplomskiProjekt"

version := "0.1"

scalaVersion := "2.11.12"
val sparkVer = "2.3.2"

// https://mvnrepository.com/artifact/org.apache.spark/spark-core
libraryDependencies += "org.apache.spark" %% "spark-core" % "2.3.2"
libraryDependencies += "org.apache.spark" % "spark-core_2.11" % sparkVer
libraryDependencies += "org.apache.spark" %% "spark-sql" % sparkVer

// mllib je STARI API za machine learning u sparku, ali ovdje ga koristimo za sparkts (ACF) i za statistiku (korelacije)
// pa sporost RDD-jeva nije problem jer se ovaj program nece izvoditi puno puta
libraryDependencies += "org.apache.spark" %% "spark-mllib" % sparkVer
// spark Time series
libraryDependencies += "com.cloudera.sparkts" % "sparkts" % "0.4.1"
