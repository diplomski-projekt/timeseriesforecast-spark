import sys

"""
Parsira datoteku s rezultatima grid searcha tako da ju mozemo upotrijebiti za 3D plot pogreske.
Argumenti: ulazna datoteka
			     izlazna datoteka
"""

input_file_name = sys.argv[1]
output_file_name = sys.argv[2]

hiperparams = []
maes = []
rmaes = []
with open(input_file_name, "r") as input_file:
    for line in input_file:
        line = line.strip()
        if (line.startswith("Features: ")):
            # ignore
            pass
        if (line.startswith("(Hiperparams")):
            hiperparams.append((line.split(",")[1], line.split(",")[2][:-1]))
        if (line.startswith("Mean Absolute Error")):
            maes.append(line.split(":")[1])
        if (line.startswith("Root Mean Squared Error")):
            rmaes.append(line.split(":")[1])

with open(output_file_name, "w") as output_file:
    output_file.write("WaterLevelLag, RainfallLag, MAE, RMSE\n")
    for (hiperparam_tuple, mae, rmae) in zip(hiperparams, maes, rmaes):
        # castanja u int i float zbog provjere parsiranih vrijednosti
        output_file.write(str(int(hiperparam_tuple[0])) + "," + str(int(hiperparam_tuple[1])) + "," + str(float(mae)) +"," + str(float(rmae)) + "\n")
