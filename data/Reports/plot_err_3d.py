from math import cos
from matplotlib import pyplot as plt
from matplotlib import  cm
from mpl_toolkits.mplot3d import  Axes3D
import numpy as np
import sys

"""
Parametar poziva skripte: naziv csv datoteke s hiperparametrima i pogreskama.
"""

def read_file(input_file_name):
  xs = []
  ys = []
  maes = []
  rmses = []
  with open(input_file_name, "r") as input_file:
    header = True
    for line in input_file:
      if header == True:
        header = False
        continue
      parts = line.split(",")
      xs.append(int(parts[0]))
      ys.append(int(parts[1]))
      maes.append(float(parts[2]))
      rmses.append(float(parts[3]))
  
  return (xs, ys,maes, rmses)
    

def plot_3d(X, Y, Z, err_metrics):
    """
    Plots a surface in 3d plot.
    :param X: mesh grid x coordinates ndarray, shape (N, N)
    :param Y: mesh grid y coordinates ndarray, shape (N, N)
    :param Z: mesh grid z values ndarray, shape (N, N)
    :return: None
    """
    fig = plt.figure()
    ax = fig.gca(projection='3d')
    ax.set_xlabel('waterLevelLag')
    ax.set_ylabel('rainfallLag')
    ax.set_zlabel(err_metrics)
    ax.set_xticks(range(1, 1 + X.shape[0]))
    ax.set_yticks(range(1, 1 + Y.shape[0]))
    
    # 3D plot
    surface = ax.plot_surface(X, Y, Z, cmap=cm.coolwarm, linewidth=10, antialiased=True)
    fig.colorbar(surface)
    
    # ucrtaj minimum
    min_coords = np.unravel_index(Z.argmin(), Z.shape)
    ax.scatter([X[min_coords]], [Y[min_coords]], [Z[min_coords]], marker='o', color='red', label='Minimum')
    # print minimuma
    print([X[min_coords]], [Y[min_coords]], [Z[min_coords]])
    
    plt.title("Ovisnost pogreške o hiperparametrima")
    plt.legend(loc='best')
    plt.show()


if __name__ == "__main__":
    input_file_name = sys.argv[1]
    result = read_file(input_file_name)
    xs = np.asarray(result[0]).reshape(15,15)
    ys = np.asarray(result[1]).reshape(15,15)
    maes = np.asarray(result[2]).reshape(15, 15)
    rmses = np.asarray(result[3]).reshape(15,15)
    plot_3d(xs, ys, maes, "MAE")
    plot_3d(xs, ys, rmses, "RMSE")