package features

import org.apache.spark.ml.feature.StringIndexer
import org.apache.spark.sql.{DataFrame, functions => F}

/**
  * Funkcije za transformaciju znacajki.
  */
object FeatureTransformer {

  /**
    * Napravi transformaciju ciklicke znacajke u clan Fourierove serije.
    * Dodaje dva nova stupca:
    * sin(2PI*k*t / T)
    * cos(2PI*k*t / T)
    *
    * @param df             DataFrame s podacima
    * @param featureColName naziv stupca koji transformiramo
    * @param k              prirodni broj, oznacava k-ti Fourierov clan
    * @param period         period podataka, npr. za godisnji period i dnevne podatke je 365
    * @return DataFrame s dva nova stupca koja predstavljaju transformirane znacajke. Imena novih stupaca su dobivena
    *         dodavanjem sufiksa "_k_sin" i "_k_cos", gdje je "k" parametar poziva metode.
    */
  def fourierTransform(df: DataFrame, featureColName: String, k: Int, period: Int): DataFrame = {
    df.withColumn("tmp", (F.col(featureColName) * 2 * math.Pi) / period)
      .withColumn(featureColName ++ "_" ++ k.toString ++ "_sin", F.sin("tmp"))
      .withColumn(featureColName ++ "_" ++ k.toString ++ "_cos", F.cos("tmp")).drop("tmp")
  }


  /**
    * Dodaje tri kategoricke znacajke za predstavljanje u kojem tromjesecju se datum nalazi. Za cetiri vrijednosti su
    * dovoljne su tri varijable jer se jedna od njih dobije kao "0 0 0", a ostale su "1 0 0", "0 1 0", "0 0 1". Time
    * sprjecavamo i linearnu zavisnost (vidi dokumentaciju)
    *
    * @param df             DataFrame s podacima
    * @param dateColName    naziv stupca koji predstavlja datum
    * @param quarterColName naziv stupca koji predstavlja tromjesecje kao vektor kategorickih varijabli
    * @return
    */
  def addYearQuarterCategoricalFeature(df: DataFrame, dateColName: String, quarterColName: String): DataFrame = {
    // odgovor hamela: https://stackoverflow.com/questions/32277576/how-to-handle-categorical-features-with-spark-ml
    var resultDf = df.withColumn("tmpQuarter", F.quarter(F.col(dateColName)))

    val indexer = new StringIndexer()
      .setInputCol("tmpQuarter")
      .setOutputCol(quarterColName)
      .fit(resultDf)

    indexer.transform(resultDf)
  }
}
