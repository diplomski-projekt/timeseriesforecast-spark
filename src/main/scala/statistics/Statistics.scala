package statistics

import scala.collection.mutable.{HashMap => MutableMap}

import com.cloudera.sparkts.EasyPlot
import com.cloudera.sparkts.stats.TimeSeriesStatisticalTests.{adftest, kpsstest}
import org.apache.spark.mllib.linalg.Vectors
import org.apache.spark.sql.{DataFrame, SparkSession}

import utils.Utils
import data.{DataLoader, DataProcessor}

/**
  * Izvodjenje statistickih testova na stacionarnost vremenskih serija.
  */
object Statistics {

  /**
    *
    * @param args plotFlag(true/false), verboseFlag(true/false)
    */
  def main(args: Array[String]): Unit = {

    // zastavica za ispis i plot
    val verboseFlag = args(0).toBoolean
    val plotFlag = args(1).toBoolean

    val spark = SparkSession.builder().appName("app").master("local[*]").getOrCreate()
    val sparCtx = spark.sparkContext
    sparCtx.setLogLevel("WARN")
    println("Spark ver" ++ spark.version)

    // Ucitavanje podataka
    val dataLoader: DataLoader = new DataLoader(spark)
    val waterLevelDfs: MutableMap[String, DataFrame] = dataLoader.readAllCsvFiles(Utils.WATER_LEVEL_DIR, ";")
    val flowDfs: MutableMap[String, DataFrame] = dataLoader.readAllCsvFiles(Utils.FLOW_DIR, ";")
    val rainfallDfs: MutableMap[String, DataFrame] = dataLoader.readAllCsvFiles(Utils.RAINFALL_DIR, ";")

    // Priprema podataka
    Utils.prepareDataFrames(waterLevelDfs, flowDfs, rainfallDfs)

    // Provjera stacionarnosti podataka
    // KPSS unit root test s p-value = 0.05 --> 95 % interval pouzdanosti
    val kpssPValue = 0.05
    /* Augmented Dickey Fuller unit test s kriticnom vrijednosti -2.89 (za vremenske serije stacionarne po trendu)
      i −3.96 (za vremenske serije koje NISU stacionarne po trendu)
     */
    val adfCriticalValue = -2.89
    val adfCriticalValueTrend = -3.96
    val maxLag = 366 // za plotanje ACF, PACF plotova
    for (mapDfs <- Seq(flowDfs, waterLevelDfs, rainfallDfs)) {
      for ((seriesName, df) <- mapDfs) {
        var currentDf = df
        val measuredValueColName = Utils.mapTimeSeriesToMeasuredValue(seriesName)
        val maxLagAdfTest = 1 + 12 * math.pow(df.count() / 100, 1.0 / 4).toInt // ad hoc odabir prema Schwertu

        // izvorni podaci bez diferenciranja
        println("\n--------------- Original data " ++ seriesName ++ " - " ++ measuredValueColName ++ " ---------------")
        val measuredValuesArray = currentDf.select(measuredValueColName).collect().map(row => row(0).toString.toDouble)

        runTests(measuredValuesArray, maxLagAdfTest, kpssPValue, adfCriticalValue, "c", verboseFlag)
        runTests(measuredValuesArray, maxLagAdfTest, kpssPValue, adfCriticalValueTrend, "ct", verboseFlag)

        if (plotFlag) {
          EasyPlot.ezplot(measuredValuesArray).saveas("data/Plots/TimeSeries/" ++ seriesName ++ ".png")
          EasyPlot.acfPlot(measuredValuesArray, maxLag, 1 - kpssPValue).saveas("data/Plots/ACF/" ++ seriesName ++ "-lag" ++ maxLag.toString ++ ".png")
          EasyPlot.pacfPlot(measuredValuesArray, maxLag, 1 - kpssPValue).saveas("data/Plots/PACF/" ++ seriesName ++ "-lag" ++ maxLag.toString ++ ".png")
        }


        /* Time differencing - stacionarnost vremenske serije
        https://otexts.org/fpp2/stationarity.html
        */
        // First order differencing
        val firstDiffMeasuredValueColName = "FirstDiff" ++ measuredValueColName
        println("\n--------------- First order differencing " ++ seriesName ++ " - " ++ firstDiffMeasuredValueColName ++ " ---------------")
        currentDf = DataProcessor.differenceTimeSeries(currentDf, measuredValueColName, Utils.DATE_COL_NAME, "Lag" ++ measuredValueColName, firstDiffMeasuredValueColName)
        var firstDiffMeasuredValuesArray = currentDf.select(firstDiffMeasuredValueColName).collect().map(row => row(0).toString.toDouble)
        // prva razlika nam ne treba jer prva lagana vrijednost zapravo ne postoji - po defaultu je 0
        firstDiffMeasuredValuesArray = firstDiffMeasuredValuesArray.slice(1, firstDiffMeasuredValuesArray.length)

        runTests(firstDiffMeasuredValuesArray, maxLagAdfTest, kpssPValue, adfCriticalValue, "c", verboseFlag)
        runTests(firstDiffMeasuredValuesArray, maxLagAdfTest, kpssPValue, adfCriticalValueTrend, "ct", verboseFlag)

        if (plotFlag) {
          EasyPlot.ezplot(firstDiffMeasuredValuesArray).saveas("data/Plots/TimeSeries/" ++ seriesName ++ "-first-order-diff.png")
          EasyPlot.acfPlot(firstDiffMeasuredValuesArray, maxLag, 1 - kpssPValue).saveas("data/Plots/ACF/" ++ seriesName ++ "-first-order-diff-lag" ++ maxLag.toString ++ ".png")
          EasyPlot.pacfPlot(firstDiffMeasuredValuesArray, maxLag, 1 - kpssPValue).saveas("data/Plots/PACF/" ++ seriesName ++ "-first-order-diff-lag" ++ maxLag.toString ++ ".png")
        }

        // Summary podataka
        println("\n--------------- Summary ---------------")
        currentDf.summary().show()
      }
    }


    spark.stop()
  }


  /**
    * Kwiatkowski, Phillips, Schmidt and Shin testiranje na nestacionarnost vremenske serije.
    * Nul-hipoteza: vremenska serija je stacionarna
    * Alternativna hipoteza: vremenska serija nije stacionarna
    *
    * @param timeSeries vremenska serija koju testiramo
    * @param pValue     p-vrijednost, manja p-vrijednost daje sigurniji i strozi test -> rijedje odbacujemo nul-hipotezu
    * @param method     "c" za provjeru stacionarnosti ili "ct" za provjeru stacionarnosti s obzirom na trend
    * @param verbose    za true se ispisuju rezultati testa u konzolu
    * @return true ako je vremenska serija NIJE stacionarna, false ako NE MOZEMO ZAKLJUCITI
    */
  def kpssStatisticalTest(timeSeries: Array[Double], pValue: Double, method: String, verbose: Boolean = false): Boolean = {
    Utils.conditionalPrint("KPSS stationary test. p-value: " ++ pValue.toString ++ ". Method: " ++ method, verbose)
    val kppsResults = kpsstest(Vectors.dense(timeSeries), method)
    val testStatistics: Double = kppsResults._1
    val criticalValsDict: Map[Double, Double] = kppsResults._2

    var nonStationary = false
    if (testStatistics > criticalValsDict(pValue)) {
      nonStationary = true
      Utils.conditionalPrint("Null hypothesis (time series is stationary) is rejected --> time series is not stationary", verbose)

    } else {
      // nismo ju odbacili, ALI ju ne smijemo ni prihvatiti, zapravo ne znamo nista
      Utils.conditionalPrint("Null hypothesis (time series is stationary) is NOT rejected --> NO conclusion", verbose)

    }
    nonStationary
  }


  /**
    * Augmented Dickey-Fuller test stacionarnosti.
    * Nul-hipoteza: vremenska serija nije stacionarna
    * Alternativna hipoteza: vremenska serija je stacionarna
    *
    * @param timeSeries    vremenska serija koju testiramo
    * @param maxLag        najveca vrijednost zaostanka (lag) vremenske serije
    * @param criticalValue kriticna vrijednost koja se koristi u testu, manja vrijednost - lakse odbacujemo hipotezu
    *                      (za kriticne vrijednosti serija s trendom ili bez njega vidjeti ovdje
    *                      https://en.wikipedia.org/wiki/Augmented_Dickey%E2%80%93Fuller_test#Examples)
    * @param method        "c" za provjeru stacionarnosti ili "ct" za provjeru stacionarnosti s obzirom na trend
    * @param verbose       za true se ispisuju rezultati testa u konzolu
    * @return par vrijednosti: (true, p-value) ako je vremenska serija stacionarna, (false, p-value) ako NE MOZEMO ZAKLJUCITI
    */
  def adfStatisticalTest(timeSeries: Array[Double], maxLag: Int, criticalValue: Double, method: String, verbose: Boolean = false): (Boolean, Double) = {
    Utils.conditionalPrint("ADF stationary test. MaxLag:" ++ maxLag.toString ++ ". Method: " ++ method, verbose)
    val adfResults = adftest(Vectors.dense(timeSeries), maxLag, method)
    val testStatistics: Double = adfResults._1
    val pValue: Double = adfResults._2
    Utils.conditionalPrint("p-value: " ++ pValue.toString, verbose)

    var stationary = false
    // mozemo i ispitivati npr. pValue <= 0.05
    if (testStatistics < criticalValue) {
      stationary = true
      Utils.conditionalPrint("Null hypothesis (time series is NOT stationary) is rejected --> time series is stationary", verbose)
    } else {
      // nismo ju odbacili, ALI ju ne smijemo ni prihvatiti, zapravo ne znamo nista
      Utils.conditionalPrint("Null hypothesis (time series is NOT stationary) is NOT rejected --> NO conclusion", verbose)
    }

    (stationary, pValue)
  }


  /**
    *
    * Pokreni KPPS test stacionarnosti za zadanu p-vrijednost.
    * Pokreci ADF test stacionarnosti pocevsi od najvece vrijednosti maxLag smanjujuci ju sve dok ne dobijes stacionarnu seriju.
    */
  private def runTests(timeSeries: Array[Double], maxLag: Int, pValue: Double, criticalVal: Double, method: String, verbose: Boolean): Unit = {
    // KPSS test
    kpssStatisticalTest(timeSeries, pValue, method, verbose)

    // ADF testing procedure - https://en.wikipedia.org/wiki/Augmented_Dickey%E2%80%93Fuller_test#Testing_procedure
    var adfResult = false
    var adfPValue: Double = -1
    var currentLag = maxLag
    do {
      val r = adfStatisticalTest(timeSeries, currentLag, criticalVal, method, verbose)
      adfResult = r._1
      adfPValue = r._2
      currentLag -= 1
    } while (!adfResult)
    println("ADF test: Time series is stationary for max lag " ++ currentLag.toString ++ ", p-value: " ++ pValue.toString)
  }
}
