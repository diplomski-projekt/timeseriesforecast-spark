package models

import java.sql.Date

import com.cloudera.sparkts.EasyPlot
import org.apache.spark.ml.evaluation.RegressionEvaluator
import org.apache.spark.ml.feature.VectorAssembler
import org.apache.spark.ml.regression.{GeneralizedLinearRegression}
import org.apache.spark.sql.{DataFrame, SparkSession}

import scala.collection.mutable.{HashMap => MutableMap}
// samo za potrebe plotanja
import data.{DataLoader, DataProcessor}
import org.apache.spark.mllib.linalg.{Vectors => mllibVectors}
import utils.Utils

/**
  * Osnovni model koji za predvidjanje koristi GLM za usporedbu tocnosti sa slozenijim modelima.
  * Model predvidja vremensku seriju pomocu prosle vrijednosti, tj. znacajka za predvidjanje x(t) je x(t-1).
  *
  */
object BaselineGeneralizedLinearModelBuilder {
  def main(args: Array[String]): Unit = {
    implicit val spark = SparkSession.builder.appName("BaselineGLModelBuilder").master("local[*]").getOrCreate()
    spark.sparkContext.setLogLevel("ERROR")
    import spark.implicits._

    // Ucitaj podatke
    val dataLoader: DataLoader = new DataLoader(spark)
    val waterLevelDfs: MutableMap[String, DataFrame] = dataLoader.readAllCsvFiles(Utils.WATER_LEVEL_DIR, ";")
    val flowDfs: MutableMap[String, DataFrame] = dataLoader.readAllCsvFiles(Utils.FLOW_DIR, ";")
    val rainfallDfs: MutableMap[String, DataFrame] = dataLoader.readAllCsvFiles(Utils.RAINFALL_DIR, ";")

    // Priprema podataka
    Utils.prepareDataFrames(waterLevelDfs, flowDfs, rainfallDfs)

    for (mapDfs <- Seq(flowDfs, waterLevelDfs, rainfallDfs)) {
      for ((seriesName, df) <- mapDfs) {
        var currentDf = df
        val measuredValueColName = Utils.mapTimeSeriesToMeasuredValue(seriesName)

        /* Podjela na train i test set - obavezno PRIJE odabira znacajki
        Zasto? U baselineu se problem javi za prvi testni primjer koji bi za znacajku dobio zadnju vrijednost iz train skupa
        sto znaci da imamo data leakage. U tom slucaju to nije veliki problem jer se radi o samo jednom podatku. No, u slozenijem
        modelu koji bi npr. racunao srednju vrijednost po tjednu i slicno, bi tu srednju vrijednost racunali nad cijelim skupom
        pa tako i nad testnim skupom sto nikako nije posteno.
        */
        val firstTrainDate = currentDf.sort("Datum").first()(0).toString
        // za test smo uzeli zadnju godinu, ALI to ne znaci da predvidjamo godinu dana unaprijed, nego to mozemo gledati kao da
        // u svakom danu u godini predvidjamo za sutrasnji dan (jer koristimo samo lag 1 koraka)
        val lastTrainDate = "2013-12-31"
        val firstTestDate = "2014-1-1"
        val lastTestDate = currentDf.sort($"Datum".desc).first()(0).toString
        var trainDf = DataProcessor.splitData(currentDf, "Datum", Date.valueOf(firstTrainDate), Date.valueOf(lastTrainDate))
        var testDf = DataProcessor.splitData(currentDf, "Datum", Date.valueOf(firstTestDate), Date.valueOf(lastTestDate))

        // Odredi znacajke - moraju biti VectorType
        trainDf = DataProcessor.lagSeries(trainDf, 1, measuredValueColName, "Datum", "Lag" ++ measuredValueColName)
        testDf = DataProcessor.lagSeries(testDf, 1, measuredValueColName, "Datum", "Lag" ++ measuredValueColName)
        val featureCols = Array("Lag" ++ measuredValueColName)
        val assemblerQueue = new VectorAssembler()
          .setInputCols(featureCols)
          .setOutputCol("Features")
        trainDf = assemblerQueue.transform(trainDf)
        testDf = assemblerQueue.transform(testDf)

        // Model
        val regressor = new GeneralizedLinearRegression()
          .setFamily("gaussian")
          .setLabelCol(measuredValueColName)
          .setFeaturesCol("Features")

        // Train
        val model = regressor.fit(trainDf)

        // Test
        var predictedDf = model.transform(testDf)

        // Test error
        val evaluator = new RegressionEvaluator()
          .setLabelCol(measuredValueColName)
          .setPredictionCol("prediction")
        val mae = evaluator.setMetricName("mae").evaluate(predictedDf)
        val rmse = evaluator.setMetricName("rmse").evaluate(predictedDf)
        val r2 = evaluator.setMetricName("r2").evaluate(predictedDf)
        println("Mean Absolute Error(FarkasicH): " ++ mae.toString)
        println("Root Mean Squared Error(FarkasicH): " ++ rmse.toString)
        println("R2: " ++ r2.toString)

        // Plot
        val trueArray = predictedDf.select(measuredValueColName).collect().map(row => row(0).toString.toDouble)
        val predictedArray = predictedDf.select("prediction").collect().map(row => row(0).toString.toDouble)
        EasyPlot.ezplot(Seq(mllibVectors.dense(trueArray), mllibVectors.dense(predictedArray)), '-')
          //.saveas("data/Plots/TestPlots/BaselineGeneralizedLinearModel/" ++ seriesName ++ ".png")
      }
    }
  }

}
