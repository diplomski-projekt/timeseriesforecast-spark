package models

import com.sun.javaws.exceptions.InvalidArgumentException
import data.DataProcessor
import org.apache.spark.ml.feature.VectorAssembler
import org.apache.spark.ml.regression.{GeneralizedLinearRegression, GeneralizedLinearRegressionModel}
import org.apache.spark.sql.{DataFrame, functions => F}
import utils.Utils

import scala.collection.immutable.HashMap

/**
  * Model za predvidanje vodostaja za 1 dan unaprijed.
  *
  * @param dfWithLabelsName naziv DataFramea cije vrijednosti vodostaja predvidamo
  * @param maxLagWaterLevel definira koliko vrijednosti vodostaja iz proslosti ulazi u znacajke
  * @param maxLagRainfall definira koliko vrijednosti padalina iz proslosti ulazi u znacajke
  * @param predictColName naziv stupca u koji ce se upisati predvidene vrijednosti
  */
class WaterLevelGLM(dfWithLabelsName: String, maxLagWaterLevel: Int, var maxLagRainfall: Int, predictColName: String) {

  private var model: Option[GeneralizedLinearRegressionModel] = Option[GeneralizedLinearRegressionModel](null)
  // Vrijednost koju predvidamo
  private val predictedMeasuredValue = Utils.mapTimeSeriesToMeasuredValue(dfWithLabelsName)


  private def getFeatures(dfs: HashMap[String, DataFrame]): DataFrame = {
    var dfWithLabels: DataFrame = dfs(dfWithLabelsName)

    // Spajanje s padalinama i vodostajima ostalih rijeka (osim dfWithLabelsName DataFrame)
    var featureCols: Array[String] = Array()
    var maxLag = -1
    for ((seriesName, df) <- dfs.filterKeys(key => key != dfWithLabelsName)) {
      val measuredVal = Utils.mapTimeSeriesToMeasuredValue(seriesName)
      if (measuredVal == Utils.WATER_LEVEL_MEASURED_VALUE) {
        maxLag = maxLagWaterLevel
      } else if (measuredVal == Utils.RAINFALL_MEASURED_VALUE) {
        maxLag = maxLagRainfall
      }
      dfWithLabels = dfWithLabels.join(
        df.withColumnRenamed(measuredVal, seriesName ++ measuredVal),
        dfWithLabels.col(Utils.DATE_COL_NAME) === df.col(Utils.DATE_COL_NAME)).drop(df.col(Utils.DATE_COL_NAME)
      )
      // Lagaj vrijednosti - to su featuri
      for (lag <- 1 to maxLag) {
        val featureName = "Lag" ++ lag.toString ++ seriesName ++ measuredVal
        dfWithLabels = DataProcessor.lagSeries(dfWithLabels, lag, seriesName ++ measuredVal, Utils.DATE_COL_NAME, featureName)
        featureCols :+= featureName
      }
    }

    // Dodavanje znacajke tjedan u godini
    dfWithLabels = dfWithLabels.withColumn("TjedanUGodini", F.weekofyear(F.col(Utils.DATE_COL_NAME)))
    featureCols :+= "TjedanUGodini"

    // Znacajke lagane vrijednosti serije koju predvidjamo
    for (lag <- 1 to maxLagWaterLevel) {
      val featureName = "Lag" ++ lag.toString ++ predictedMeasuredValue
      dfWithLabels = DataProcessor.lagSeries(dfWithLabels, lag, predictedMeasuredValue, Utils.DATE_COL_NAME, featureName)
      featureCols :+= featureName
    }

    // Slozi znacajke u vektor
    val assemblerQueue = new VectorAssembler()
      .setInputCols(featureCols)
      .setOutputCol("Znacajke")

    dfWithLabels = assemblerQueue.transform(dfWithLabels)

    dfWithLabels
  }

  /**
    * Uci model generalizirane linearne regresije.
    * Prilikom ucenja stvaraju se stupci TjedanUGodini i Znacajke pa ulazni podaci ne smiju sadrzavati stupce s tim imenom.
    *
    * @param trainDfs HahsMapa s imenovanim podacima (DataFrameovima) za ucenje
    * @throws InvalidArgumentException ako trainDfs ne sadrzi podatke imenovane s dfWithLabelsName
    *                                  ili trainDfs sadrzi stupce TjedanUGodini ili Znacajke
    */
  @throws(classOf[InvalidArgumentException])
  def fit(trainDfs: HashMap[String, DataFrame]): Unit = {
    // Provjera parametara
    if (trainDfs.get(dfWithLabelsName).isEmpty) {
      throw new InvalidArgumentException(Array("trainDfs mora sadrzavati podatke imenovane s " + dfWithLabelsName))
    }
    for (df <- trainDfs.values) {
      if (df.columns.contains("TjedanUGodini")) {
        throw new InvalidArgumentException(Array("trainDfs ne smije sadrzavati podatke sa stupcem TjedanUGodini"))
      }
      if (df.columns.contains("Znacajke")) {
        throw new InvalidArgumentException(Array("trainDfs ne smije sadrzavati podatke sa stupcem Znacajke"))
      }
    }

    val trainDf: DataFrame = getFeatures(trainDfs)

    // Model
    val regressor = new GeneralizedLinearRegression()
      .setFamily("gaussian")
      .setLabelCol(predictedMeasuredValue)
      .setFeaturesCol("Znacajke")
      .setPredictionCol(predictColName)

    // Train
    model = Option[GeneralizedLinearRegressionModel](regressor.fit(trainDf))
  }


  /**
    * Predvidanje ce se napraviti za sve zadane podatke, osim za prvih max(maxLagWaterLevel, maxLagRainfall) podataka
    * jer za te podatke jer nemamo od kuda odrediti zaostale (lag) vrijednosti pa bi predvidanja bila netocna.
    *
    * @param dfsToPredict HahsMapa s imenovanim podacima (DataFrameovima) za predvidanje. Podaci moraju sadrzavati barem
    *                     max(maxLagWaterLevel, maxLagRainfall) zapisa jer se ti zapisi koriste kao znacajke pri predvidanju
    * @throws InvalidArgumentException ako svaki DataFrame u dfsToPredict ne sadrzi barem
    *                                  max(maxLagWaterLevel, maxLagRainfall) zapisa ili sadrzi
    *                                  stupac s nazivom predictColName
    * @return DataFrame sa stupcem datuma i predvidenom vrijednosti u stupcu predictColName
    */
  @throws(classOf[InvalidArgumentException])
  def transform(dfsToPredict: HashMap[String, DataFrame]): DataFrame = {
    // Provjera parametara
    for ((_, df) <- dfsToPredict) {
      if (df.count() < math.max(maxLagWaterLevel, maxLagRainfall)) {
        throw new InvalidArgumentException(Array("Podaci moraju sadrzavati barem max(maxLagWaterLevel, maxLagRainfall) = " + math.max(maxLagWaterLevel, maxLagRainfall).toString + " zapisa"))
      }
      if (df.columns.contains(predictColName)) {
        throw new InvalidArgumentException(Array("trainDfs ne smije sadrzavati podatke sa stupcem " + predictColName))
      }
    }

    val dfToPredict: DataFrame = getFeatures(dfsToPredict)
    var predictedDf = model.get.transform(dfToPredict)

    // Izbaci prvih max(maxLagWaterLevel, maxLagRainfall) redaka za koje predikcije nisu tocne
    // implementacijski - sortiraj silazno pa uzmi (N_ukupno - max(maxLagWaterLevel, maxLagRainfall)) prvih redaka
    predictedDf = predictedDf
      .sort(F.col(Utils.DATE_COL_NAME).desc)
      .limit(predictedDf.count().toInt - math.max(maxLagWaterLevel, maxLagRainfall))

    predictedDf.select(Utils.DATE_COL_NAME, predictedMeasuredValue, predictColName)
  }


}
