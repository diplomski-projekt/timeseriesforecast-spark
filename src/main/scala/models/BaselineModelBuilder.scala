package models

import java.sql.Date

import com.cloudera.sparkts.EasyPlot
import org.apache.spark.ml.evaluation.RegressionEvaluator
import org.apache.spark.sql.{DataFrame, SparkSession}

import scala.collection.mutable.{HashMap => MutableMap}
// samo za potrebe plotanja
import data.{DataLoader, DataProcessor}
import org.apache.spark.mllib.linalg.{Vectors => mllibVectors}
import utils.Utils


/**
  * Osnovni model za usporedbu tocnosti sa slozenijim modelima.
  * Model predvidja vremensku seriju pomocu prosle vrijednosti, tj. predvidjena vrijednost u trenutku t je vrijednost iz trenutka t-1.
  *
  * Iako po grafovima i ispisu pogreske mozemo pomisliti kako je ovaj model mocan i dobro predvidja to nije istina.
  * Sve sto on radi je da za sutrasnji dan predvidi vrijednost koja je bila danas. Za tu funkcionalnost nam ne treba
  * nikakav model predvidjanja.
  *
  */
object BaselineModelBuilder {
  def main(args: Array[String]): Unit = {
    implicit val spark = SparkSession.builder.appName("BaselineModelBuilder").master("local[*]").getOrCreate()
    spark.sparkContext.setLogLevel("ERROR")
    import spark.implicits._

    // Ucitaj podatke
    val dataLoader: DataLoader = new DataLoader(spark)
    val waterLevelDfs: MutableMap[String, DataFrame] = dataLoader.readAllCsvFiles(Utils.WATER_LEVEL_DIR, ";")
    val flowDfs: MutableMap[String, DataFrame] = dataLoader.readAllCsvFiles(Utils.FLOW_DIR, ";")
    val rainfallDfs: MutableMap[String, DataFrame] = dataLoader.readAllCsvFiles(Utils.RAINFALL_DIR, ";")

    // Priprema podataka
    Utils.prepareDataFrames(waterLevelDfs, flowDfs, rainfallDfs)

    for (mapDfs <- Seq(flowDfs, waterLevelDfs, rainfallDfs)) {
      for ((seriesName, df) <- mapDfs) {
        var currentDf = df
        val measuredValueColName = Utils.mapTimeSeriesToMeasuredValue(seriesName)

        val firstTrainDate = currentDf.sort("Datum").first()(0).toString
        // za test smo uzeli zadnju godinu, ALI to ne znaci da predvidjamo godinu dana unaprijed, nego to mozemo gledati kao da
        // u svakom danu u godini predvidjamo za sutrasnji dan (jer koristimo samo lag 1 koraka)
        val from = "2014-1-1"
        val to = currentDf.sort($"Datum".desc).first()(0).toString
        currentDf = DataProcessor.splitData(currentDf, "Datum", Date.valueOf(from), Date.valueOf(to))

        // Predict - uzmi prethodnu vrijednost
        currentDf = DataProcessor.lagSeries(currentDf,1,  measuredValueColName, "Datum", "prediction")

        // Prediction error
        val evaluator = new RegressionEvaluator()
          .setLabelCol(measuredValueColName)
          .setPredictionCol("prediction")
          .setMetricName("mae")
        val mae = evaluator.evaluate(currentDf)
        println("Mean Absolute Error(" ++ seriesName ++ "): " ++ mae.toString)

        // Plot
        val trueArray = currentDf.select(measuredValueColName).collect().map(row => row(0).toString.toDouble)
        val predictedArray = currentDf.select("prediction").collect().map(row => row(0).toString.toDouble)
        EasyPlot.ezplot(Seq(mllibVectors.dense(trueArray), mllibVectors.dense(predictedArray)), '-')
          //.saveas("data/Plots/TestPlots/Baseline/" ++ seriesName ++ ".png")
      }
    }
  }
}
