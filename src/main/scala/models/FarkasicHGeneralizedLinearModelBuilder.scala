package models

import java.sql.Date

import com.cloudera.sparkts.EasyPlot
import data.{DataLoader, DataProcessor}
import features.FeatureTransformer
import org.apache.spark.ml.evaluation.RegressionEvaluator
import org.apache.spark.ml.feature.VectorAssembler
import org.apache.spark.ml.regression.GeneralizedLinearRegression
import org.apache.spark.sql.{DataFrame, SparkSession, functions => F}
import utils.Utils

import scala.collection.mutable.ArrayBuffer
// samo za potrebe plotanja
import org.apache.spark.mllib.linalg.{Vectors => mllibVectors}

import scala.collection.mutable.{HashMap => MutableMap}

/**
  * Model za predvidjanje vodostaja na mjernoj postaji Farkasic.
  * Algoritam slucajnih suma koristi zaostale (lag) vrijednosti na ostalim mjernim postajama vodostaja i padalina kao znacajke.
  *
  */
object FarkasicHGeneralizedLinearModelBuilder {
  /**
    *
    * @param args maxLagVodostaj - prirodni broj koji govori koliko zaostalih vrijednosti treba uzeti u znacajke vodostaja.
    *             maxLagPadaline - prirodni broj koji govori koliko zaostalih vrijednosti treba uzeti u znacajke padalina.
    */
  def main(args: Array[String]): Unit = {
    implicit val spark = SparkSession.builder.appName("GLModelBuilder").master("local[*]").getOrCreate()
    spark.sparkContext.setLogLevel("ERROR")
    import spark.implicits._

    var maxLagWaterLevel: Int = args(0).toInt
    var maxLagRainfall: Int = args(1).toInt

    // Hiperparametri - grid search
    var maes = ArrayBuffer[(Double, Int, Int)]()
    var rmses = ArrayBuffer[(Double, Int, Int)]()

    for (lagWaterLevel <- 1 to maxLagWaterLevel) {
      for (lagRainfall <- 1 to maxLagRainfall) {
        // Ucitaj podatke
        val dataLoader: DataLoader = new DataLoader(spark)
        val waterLevelDfs: MutableMap[String, DataFrame] = dataLoader.readAllCsvFiles(Utils.WATER_LEVEL_DIR, ";")
        val flowDfs: MutableMap[String, DataFrame] = dataLoader.readAllCsvFiles(Utils.FLOW_DIR, ";")
        val rainfallDfs: MutableMap[String, DataFrame] = dataLoader.readAllCsvFiles(Utils.RAINFALL_DIR, ";")

        // Priprema podataka
        Utils.prepareDataFrames(waterLevelDfs, flowDfs, rainfallDfs)

        var farkasicH = waterLevelDfs("FarkasicH")

        // Spajanje s padalinama i vodostajima ostalih rijeka (osim FarkasicH)
        var featureCols: Array[String] = Array()
        var maxLag = -1
        for (dfsMap <- Seq(rainfallDfs, waterLevelDfs.filterKeys(key => key != "FarkasicH"))) {
          for ((seriesName, df) <- dfsMap) {
            val measuredVal = Utils.mapTimeSeriesToMeasuredValue(seriesName)
            if (measuredVal == Utils.WATER_LEVEL_MEASURED_VALUE) {
              maxLag = lagWaterLevel
            } else if (measuredVal == Utils.RAINFALL_MEASURED_VALUE) {
              maxLag = lagRainfall
            }
            farkasicH = farkasicH.join(
              df.withColumnRenamed(measuredVal, seriesName ++ measuredVal),
              farkasicH.col(Utils.DATE_COL_NAME) === df.col(Utils.DATE_COL_NAME)).drop(df.col(Utils.DATE_COL_NAME)
            )
            // Lagaj vrijednosti - to su featuri
            for (lag <- 1 to maxLag) {
              val featureName = "Lag" ++ lag.toString ++ seriesName ++ measuredVal
              farkasicH = DataProcessor.lagSeries(farkasicH, lag, seriesName ++ measuredVal, Utils.DATE_COL_NAME, featureName)
              featureCols :+= featureName
            }
          }
        }

        // Dodavanje znacajke tjedan u godini
        farkasicH = farkasicH.withColumn("TjedanUGodini", F.weekofyear(F.col(Utils.DATE_COL_NAME)))
        featureCols :+= "TjedanUGodini"

        /*
        // sinus, cosinus transformacija vremena
        farkasicH = farkasicH.withColumn("DanUGodini", F.dayofyear(F.col(Utils.DATE_COL_NAME)))
        for (k <- 1 to 20) { // ovdje 20 staviti kao argument poziva programa
          farkasicH = FeatureTransformer.fourierTransform(farkasicH, "DanUGodini", k, 366)
          featureCols :+= "DanUGodini_" ++ k.toString ++ "_sin"
          featureCols :+= "DanUGodini_" ++ k.toString ++ "_cos"
        }
        // transformacijom datuma u sin, cos smo izgubili informaciju datuma i godisnjeg doba (tromjesecje) sto nam je bitno za predvidanje
        farkasicH = FeatureTransformer.addYearQuarterCategoricalFeature(farkasicH, Utils.DATE_COL_NAME, "TromjesecjeKategoricke")
        featureCols :+= "TromjesecjeKategoricke"
        */



        val firstTrainDate = farkasicH.sort(Utils.DATE_COL_NAME).first()(0).toString
        val lastTrainDate = "2013-12-31"
        val firstTestDate = "2014-1-1"
        val lastTestDate = farkasicH.sort(F.col(Utils.DATE_COL_NAME).desc).first()(0).toString
        var farkasicTrain = DataProcessor.splitData(farkasicH, Utils.DATE_COL_NAME, Date.valueOf(firstTrainDate), Date.valueOf(lastTrainDate))
        var farkasicTest = DataProcessor.splitData(farkasicH, Utils.DATE_COL_NAME, Date.valueOf(firstTestDate), Date.valueOf(lastTestDate))

        // Znacajke lagane vrijednosti serije koju predvidjamo
        for (lag <- 1 to lagWaterLevel) {
          val featureName = "Lag" ++ lag.toString ++ Utils.WATER_LEVEL_MEASURED_VALUE
          farkasicTrain = DataProcessor.lagSeries(farkasicTrain, lag, Utils.WATER_LEVEL_MEASURED_VALUE, Utils.DATE_COL_NAME, featureName)
          farkasicTest = DataProcessor.lagSeries(farkasicTest, lag, Utils.WATER_LEVEL_MEASURED_VALUE, Utils.DATE_COL_NAME, featureName)
          featureCols :+= featureName
        }

        println("Features: ", featureCols.toList)
        val assemblerQueue = new VectorAssembler()
          .setInputCols(featureCols)
          .setOutputCol("Znacajke")
        farkasicTrain = assemblerQueue.transform(farkasicTrain)
        farkasicTest = assemblerQueue.transform(farkasicTest)

        // Model
        val regressor = new GeneralizedLinearRegression()
          .setFamily("gaussian")
          .setLabelCol(Utils.WATER_LEVEL_MEASURED_VALUE)
          .setFeaturesCol("Znacajke")

        // Train
        val model = regressor.fit(farkasicTrain)

        // Test
        val predictedDf = model.transform(farkasicTest)

        // Test error
        val evaluator = new RegressionEvaluator()
          .setLabelCol(Utils.WATER_LEVEL_MEASURED_VALUE)
          .setPredictionCol("prediction")
        val mae = evaluator.setMetricName("mae").evaluate(predictedDf)
        val rmse = evaluator.setMetricName("rmse").evaluate(predictedDf)
        val r2 = evaluator.setMetricName("r2").evaluate(predictedDf)
        maes.append((mae, lagWaterLevel, lagRainfall))
        rmses.append((rmse, lagWaterLevel, lagRainfall))
        println("Hiperparams", lagWaterLevel, lagRainfall)
        println("Mean Absolute Error(FarkasicH): " ++ mae.toString)
        println("Root Mean Squared Error(FarkasicH): " ++ rmse.toString)
        println("R2: " ++ r2.toString)

        // Plot
        val trueArray = predictedDf.select(Utils.WATER_LEVEL_MEASURED_VALUE).collect().map(row => row(0).toString.toDouble)
        val predictedArray = predictedDf.select("prediction").collect().map(row => row(0).toString.toDouble)
        EasyPlot.ezplot(Seq(mllibVectors.dense(trueArray), mllibVectors.dense(predictedArray)), '-')
        //.saveas("data/Plots/TestPlots/GLM/FarkasicH.png")
      }
    }

    println("Min MAE", maes.minBy(tuple => tuple._1))
    println("Min RMSE", rmses.minBy(tuple => tuple._1))
  }
}
