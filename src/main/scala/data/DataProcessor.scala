package data

import java.sql.Date

import org.apache.spark.sql.expressions.Window
import org.apache.spark.sql.types.DoubleType
import org.apache.spark.sql.{DataFrame, SparkSession, functions => F}

/**
  * Objekt za obradu podataka.
  */
object DataProcessor {
  /**
    * Pretvara stupca tipa StringType u tip DateType.
    * Nakon ove pretvorbe datum je u formatu yyyy-MM-dd
    *
    * @param df          DataFrame sa stupcem prosljedjenim kao drugi parametar
    * @param dateColName naziv stupca kojeg castamo
    * @param dateFormat  format zapisa datuma (npr. dd.MM.yyyy, MM-dd-yy, ...)
    * @return DataFrame s castanim stupcem datuma
    */
  def castStringDateToDateCol(df: DataFrame, dateColName: String, dateFormat: String): DataFrame = {
    df.withColumn(
      dateColName, F.unix_timestamp(F.col(dateColName), dateFormat)
        .cast("timestamp").cast("date"))
  }

  /**
    * Pretvara stupca tipa StringType u tip DoubleType.
    *
    * @param df             DataFrame sa stupcem prosljedjenim kao drugi parametar
    * @param featureColName naziv stupca kojeg castamo
    * @return DataFrame s castanim stupcem
    */
  def castStringToDoubleCol(df: DataFrame, featureColName: String): DataFrame = {
    df.withColumn(featureColName, F.col(featureColName).cast(DoubleType))
  }


  /**
    * Napravi novi stupac sa zaostalim vrijednostima odredjene znacajke.
    *
    * @param df             DataFrame u ciji stupac diferenciramo
    * @param offset         broj koraka za koje zaostali podaci zaostaju
    * @param featureColName naziv stupca cije zaostale vrijednosti odredjujemo
    * @param orderByColName naziv stupca po kojem se podaci sortiraju prilikom izrade vremenskog prozora
    * @param lagFeatureName naziv novog stupca sa zaostalim (laganim) vrijednostima
    * @return DataFrame koji sadrzi novi stupac zaostalih vrijednosti
    */
  def lagSeries(df: DataFrame, offset: Int, featureColName: String, orderByColName: String, lagFeatureName: String): DataFrame = {
    require(offset > 0)
    val window = Window.orderBy(orderByColName)
    val dfLag = df.withColumn(lagFeatureName, F.lag(featureColName, offset, 0).over(window))

    dfLag
  }


  /**
    * Diferenciranje vremenske serije: delta_x = x(t) - x(t-1).
    * Uzastopnom primjenom mozemo ostvariti i diferenciranje viseg reda.
    *
    * @param df                 DataFrame u ciji stupac diferenciramo
    * @param featureColName     naziv stupca koji diferenciramo
    * @param orderByColName     naziv stupca po kojem se podaci sortiraju prilikom izrade vremenskog prozora
    * @param lagFeatureName     naziv novog stupca sa zaostalim (laganim) vrijednostima
    * @param diffFeatureColName naziv novog stupca diferenciranih znacajki
    * @return DataFrame koji sadrzi 2 nova stupca: stupac zaostalih vrijednosti, diferencirani stupac
    */
  def differenceTimeSeries(df: DataFrame, featureColName: String, orderByColName: String, lagFeatureName: String, diffFeatureColName: String): DataFrame = {
    // time differencing - stacionarnost vremenske serije
    // https://otexts.org/fpp2/stationarity.html
    val dfLag = lagSeries(df, 1, featureColName, orderByColName, lagFeatureName)
    val dfDiff = dfLag.withColumn(diffFeatureColName, F.col(featureColName) - F.col(lagFeatureName))

    dfDiff
  }


  /**
    * Iz podataka odabire podatke za treniranje.
    * Format datuma mora biti isti kakav je i u podacima.
    *
    * @param df          podaci
    * @param dateColName ime stupca po kojem sortiramo i dijelimo podatke
    * @param dateFrom    datum podatka (ukljucujuci) od kojeg se podaci uzimaju u skup za treniranje
    * @param dateTo      datum podatka (ukljucujuci) do kojeg se podaci uzimaju u skup za treniranje
    * @return DataFrame podataka za treniranje
    */
  def splitData(df: DataFrame, dateColName: String, dateFrom: Date, dateTo: Date): DataFrame = {
    df.filter(F.col(dateColName) >= F.lit(dateFrom))
      .filter(F.col(dateColName) <= F.lit(dateTo))
  }

/**
  * DEMO
  */
  def main(args: Array[String]): Unit = {
    val spark = SparkSession.builder().appName("app").master("local[*]").getOrCreate()
    val sparCtx = spark.sparkContext
    sparCtx.setLogLevel("WARN")
    println("Spark ver" ++ spark.version)

    // Ucitavanje i priprema podataka
    var farkasicWL = spark.read.option("header", true).option("sep", ";").csv("./data/Kupa/WaterLevel/FarkasicH.csv")
    farkasicWL = castStringDateToDateCol(farkasicWL, "Datum", "dd.MM.yyyy")
    farkasicWL = castStringToDoubleCol(farkasicWL, "Vodostaj")
    farkasicWL.show(10)

  }
}
