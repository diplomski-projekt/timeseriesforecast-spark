package data

import java.io.File

import org.apache.spark.sql.{DataFrame, SparkSession}

import scala.collection.mutable.{HashMap => MutableMap}

/**
  * Klasa za ucitavanje svih podataka.
  *
  * @param spark SparkSession objekt
  */
class DataLoader(spark: SparkSession) {

  /**
    * Funkcija vraca listu putanja do svih datoteka u zadanom direktoriju.
    *
    * @param directoryPath putanja do direktorija
    * @return lista putanja do svih datoteka u direktoriju
    */
  def getListOfFiles(directoryPath: String): List[String] = {
    val file = new File(directoryPath)
    file
      .listFiles
      .filter(_.isFile)
      .map(_.getPath).toList
  }


  /**
    * Funkcija cita sve CSV datoteke iz nekog direktorija.
    *
    * @param directoryPath putanja do direktorija koji sadrzi CSV datoteke za procitati
    * @param separator separator kojim su podaci odvojeni (npr. "," ili ";" ...)
    * @param header postoji li header u datoteci
    * @return mapa s imenima DataFrameova i DataFrameovima: [key=timeSeriesName, val=DataFrame]
    */
  def readAllCsvFiles(directoryPath: String, separator: String, header: Boolean = true): MutableMap[String, DataFrame] = {
    var dataFrameMap = MutableMap[String, DataFrame]()

    // iteriraj po svim csv datotekama
    for (filePath <- getListOfFiles(directoryPath)) {
      val fileName = filePath.split('\\').last.split('.')(0)
      dataFrameMap += (fileName -> spark.read.option("header", header).option("sep", separator).csv(filePath))
    }
    dataFrameMap
  }
}
