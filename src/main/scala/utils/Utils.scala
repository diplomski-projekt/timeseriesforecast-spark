package utils

import scala.collection.mutable.{HashMap => MutableMap}
import org.apache.spark.sql.DataFrame

import data.DataProcessor

/**
  * Zajednicke specificne metode koje bismo inace stavili u klase pod oznaku private.
  * Zajednicke staticke varijable.
  *
  * U slucaju promjene naziva datoteka, stupaca u datotekama dovoljno je promjeniti nekoliko statickih varijabli ili
  * dijlova ovih funkcija te tako izbjegavamo potrebu za novom implementacijom modela.
  */
object Utils {
  val WATER_LEVEL_DIR = "data/Kupa/WaterLevel"
  val FLOW_DIR = "data/Kupa/Flow"
  val RAINFALL_DIR = "data/Kupa/Rainfall"

  val WATER_LEVEL_MEASURED_VALUE = "Vodostaj"
  val FLOW_MEASURED_VALUE = "Protok"
  val RAINFALL_MEASURED_VALUE = "Padaline"

  val DATE_COL_NAME = "Datum"

  /**
    * Mapiranje naziva serija u mjerenu vrijednost (Vodostaj, Protok, Padaline)
    * @param timeSeriesName naziv vremenske serije - ako zavrsava sa "H" radi se o vodostaju, ako zavrsava na "Q" radi
    *                       se o protoku, ako zavrsava na "R" radi se o padalinama
   */
  def mapTimeSeriesToMeasuredValue(timeSeriesName: String): String = {
    var measuredVal = ""
    if (timeSeriesName.endsWith("H")) {
      measuredVal = WATER_LEVEL_MEASURED_VALUE
    } else if (timeSeriesName.endsWith("Q")) {
      measuredVal = FLOW_MEASURED_VALUE
    } else if (timeSeriesName.endsWith("R")) {
      measuredVal = RAINFALL_MEASURED_VALUE
    }
    measuredVal
  }

  /**
    * In-place priprema podataka.
    *
    * @param waterLevelDfs mapa s DataFrameovima vodostaja
    * @param flowDfs       mapa s DataFrameovima protoka
    * @param rainfallDfs   mapa s DataFrameovima padalina
    */
  def prepareDataFrames(waterLevelDfs: MutableMap[String, DataFrame], flowDfs: MutableMap[String, DataFrame], rainfallDfs: MutableMap[String, DataFrame]) {
    for ((k, v) <- waterLevelDfs) {
      // cast datuma iz stringa u date
      waterLevelDfs(k) = DataProcessor.castStringDateToDateCol(waterLevelDfs(k), DATE_COL_NAME, "dd.MM.yyyy")
      waterLevelDfs(k) = DataProcessor.castStringToDoubleCol(waterLevelDfs(k), WATER_LEVEL_MEASURED_VALUE)
    }

    for ((k, v) <- flowDfs) {
      // cast datuma iz stringa u date
      flowDfs(k) = DataProcessor.castStringDateToDateCol(flowDfs(k), DATE_COL_NAME, "dd.MM.yyyy")
      flowDfs(k) = DataProcessor.castStringToDoubleCol(flowDfs(k), FLOW_MEASURED_VALUE)
    }

    for ((k, v) <- rainfallDfs) {
      // cast datuma iz stringa u date
      rainfallDfs(k) = DataProcessor.castStringDateToDateCol(rainfallDfs(k), DATE_COL_NAME, "yyyy-MM-dd")
      rainfallDfs(k) = DataProcessor.castStringToDoubleCol(rainfallDfs(k), RAINFALL_MEASURED_VALUE)
    }
  }


  /**
    * Ispis u konzolu, ako je zastavica postavljena na true.
    */
  def conditionalPrint(string: String, flag: Boolean): Unit = {
    if (flag) {
      println(string)
    }
  }

  /**
    * DEMO
    */
  def main(args: Array[String]): Unit = {
    println(mapTimeSeriesToMeasuredValue("FarkasicH"))
    println(mapTimeSeriesToMeasuredValue("FarkasicQ"))
    println(mapTimeSeriesToMeasuredValue("BosiljevoR"))
  }
}
