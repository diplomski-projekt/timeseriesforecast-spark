import java.sql.Date

import data.DataLoader
import models.WaterLevelGLM
import org.apache.spark.ml.evaluation.RegressionEvaluator
import org.apache.spark.sql.{DataFrame, SparkSession, functions => F}
import utils.Utils

import scala.collection.immutable.{Map, HashMap => ImmutableMap}
import scala.collection.mutable.{HashMap => MutableMap}

object Main {

  def main(args: Array[String]): Unit = {
    implicit val spark = SparkSession.builder.appName("Main").master("local[*]").getOrCreate()
    spark.sparkContext.setLogLevel("ERROR")

    // Ucitaj podatke
    val dataLoader: DataLoader = new DataLoader(spark)
    val waterLevelDfs: MutableMap[String, DataFrame] = dataLoader.readAllCsvFiles(Utils.WATER_LEVEL_DIR, ";")
    val flowDfs: MutableMap[String, DataFrame] = dataLoader.readAllCsvFiles(Utils.FLOW_DIR, ";")
    val rainfallDfs: MutableMap[String, DataFrame] = dataLoader.readAllCsvFiles(Utils.RAINFALL_DIR, ";")

    // Priprema podataka
    Utils.prepareDataFrames(waterLevelDfs, flowDfs, rainfallDfs)

    // Hiperparametri
    val maxLagWaterLevel = 3
    val maxLagRainfall = 1

    //TRENIRANJE
    // Podaci za treniranje
    val waterLevelMeasuringStations = "FarkasicH"
    // Trik za dobivanje immutable.Map iz mutable.HashMap
    val trainDfs = ImmutableMap[String, DataFrame]() ++
      waterLevelDfs.map(kv => (kv._1, kv._2.where(F.col(Utils.DATE_COL_NAME) <= Date.valueOf("2014-11-30")))) ++
      rainfallDfs.map(kv => (kv._1, kv._2.where(F.col(Utils.DATE_COL_NAME) <= Date.valueOf("2014-11-30"))))

    val farkasicHModel = new WaterLevelGLM(waterLevelMeasuringStations, maxLagWaterLevel, maxLagRainfall, "Predvideno")
    farkasicHModel.fit(trainDfs.asInstanceOf[ImmutableMap[String, DataFrame]])


    // PREDVIDANJE za 1 dan unaprijed - provodi se nad svim oznacenim podacima - ovdje smo uzeli zadnji mjesec podataka
    // Povijesni podaci za predvidanje - barem max(maxLagWaterLevel, maxLagRainfall) zapisa u svakom DF-u
    val predictDfs: Map[String, DataFrame] = ImmutableMap[String, DataFrame]() ++
      waterLevelDfs.map(kv => (kv._1, kv._2.where(F.col(Utils.DATE_COL_NAME) > Date.valueOf("2014-11-30")))) ++
      rainfallDfs.map(kv => (kv._1, kv._2.where(F.col(Utils.DATE_COL_NAME) > Date.valueOf("2014-11-30"))))

    val predictedDf = farkasicHModel.transform(predictDfs.asInstanceOf[ImmutableMap[String, DataFrame]])

    predictedDf.show(100)


    // Test error - ako predvidamo za buducnost ovo izbaciti jer nemamo stvarne vrijednosti
    val evaluator = new RegressionEvaluator()
      .setLabelCol(Utils.WATER_LEVEL_MEASURED_VALUE)
      .setPredictionCol("Predvideno")
    val mae = evaluator.setMetricName("mae").evaluate(predictedDf)
    val rmse = evaluator.setMetricName("rmse").evaluate(predictedDf)
    val r2 = evaluator.setMetricName("r2").evaluate(predictedDf)
    println("Mean Absolute Error: " ++ mae.toString)
    println("Root Mean Squared Error: " ++ rmse.toString)
    println("R2: " ++ r2.toString)
  }
}
